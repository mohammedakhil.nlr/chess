package com.sapient.chess.chess.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class UserGameData {
    private final String id;
    private final User user;
    private final GameData gameData;
    private final String color;
    private final String result;

    public UserGameData(String id, User user, GameData gameData, String color, String result) {
        this.id = id;
        this.user = user;
        this.gameData = gameData;
        this.color = color;
        this.result = result;
    }

    public String getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public GameData getGameData() {
        return gameData;
    }

    public String getColor() {
        return color;
    }

    public String getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        UserGameData that = (UserGameData) o;

        return new EqualsBuilder()
                .append(user, that.user)
                .append(gameData, that.gameData)
                .append(color, that.color)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(user)
                .append(gameData)
                .append(color)
                .toHashCode();
    }
}
