package com.sapient.chess.chess.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class UserMoveData {
    private final String userMoveId;
    private final UserGameData userGameData;
    private final String move;

    public UserMoveData(String userMoveId, UserGameData userGameData, String move) {
        this.userMoveId = userMoveId;
        this.userGameData = userGameData;
        this.move = move;
    }

    public String getUserMoveId() {
        return userMoveId;
    }

    public UserGameData getUserGameData() {
        return userGameData;
    }

    public String getMove() {
        return move;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        UserMoveData that = (UserMoveData) o;

        return new EqualsBuilder()
                .append(userMoveId, that.userMoveId)
                .append(userGameData, that.userGameData)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(userMoveId)
                .append(userGameData)
                .toHashCode();
    }
}
