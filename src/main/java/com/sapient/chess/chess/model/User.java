package com.sapient.chess.chess.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

public class User implements Serializable {
    public final String password;
    public final String email;
    public final boolean active;

    public User(String password, String email, boolean active) {
        this.password = password;
        this.email = email;
        this.active = active;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }


    public boolean isActive() {
        return active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        User that = (User) o;

        return new EqualsBuilder()
                .append(email, that.email)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(email)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", active=" + active +
                '}';
    }
}
