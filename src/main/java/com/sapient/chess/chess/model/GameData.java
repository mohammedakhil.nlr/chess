package com.sapient.chess.chess.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

public class GameData implements Serializable {
    private final String gameId;
    private final String gameStatus;

    public GameData(String gameId, String gameStatus) {
        this.gameId = gameId;
        this.gameStatus = gameStatus;
    }

    public String getGameId() {
        return gameId;
    }

    public String getGameStatus() {
        return gameStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GameData gameData = (GameData) o;

        return new EqualsBuilder()
                .append(gameId, gameData.gameId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(gameId)
                .toHashCode();
    }
}
