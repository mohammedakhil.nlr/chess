package com.sapient.chess.chess.api;

public enum Colour {
    BLACK, WHITE
}
