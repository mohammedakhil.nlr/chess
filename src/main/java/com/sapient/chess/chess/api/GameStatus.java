package com.sapient.chess.chess.api;

public enum GameStatus {
    Open("O"),
    Playing("P"),
    Closed("C");

    private final String code;

    GameStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static GameStatus fromCode(String value) {
        for (GameStatus statusCode : values()) {
            if (statusCode.getCode().equals(value)) {
                return statusCode;
            }
        }
        return null;
    }

}
