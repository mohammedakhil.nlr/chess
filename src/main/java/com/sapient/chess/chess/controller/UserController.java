package com.sapient.chess.chess.controller;

import com.sapient.chess.chess.exception.DataCreationException;
import com.sapient.chess.chess.exception.DataExistsException;
import com.sapient.chess.chess.exception.InvalidDataException;
import com.sapient.chess.chess.requests.UserRequest;
import com.sapient.chess.chess.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
public class UserController {
    private final UserService userService;

    @Inject
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/createUser", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<String> createUser(@RequestBody UserRequest userRequest) throws Exception{
        userService.addUser(userRequest);
        return new ResponseEntity<String>("Successfully Added", HttpStatus.CREATED);
    }

    @ExceptionHandler({DataCreationException.class, DataExistsException.class, InvalidDataException.class})
    @ResponseBody
    public ResponseEntity<String> handleOperationException(Exception exception) throws Exception{
        return new ResponseEntity<String >(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
