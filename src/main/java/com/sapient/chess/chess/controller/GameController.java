package com.sapient.chess.chess.controller;

import com.sapient.chess.chess.service.GameService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
public class GameController {
    private final GameService gameService;

    @Inject
    public GameController(GameService gameService){
        this.gameService = gameService;
    }


    @RequestMapping(value = "/startGame/{emailId}/{colour}", method = RequestMethod.GET)
    public ResponseEntity<String> newGameResponse(
            @PathVariable("emailId") String emailId,
            @PathVariable("colour") String colour) {
        //look for existing games or create new game
        String msg = gameService.assignGame(emailId, colour);


        return new ResponseEntity<>(msg, HttpStatus.OK);
    }


    @RequestMapping(value = "/joinGame/{gameId}/{emailId}", method = RequestMethod.GET)
    public ResponseEntity<String> joinGame(
            @PathVariable("gameId") String gameId,
            @PathVariable("emailId") String emailId) {
        return new ResponseEntity<>("Msg : joined Game", HttpStatus.OK);
    }

    @RequestMapping(value = "/move/{gameId}/{emailId}/{move}", method = RequestMethod.GET)
    public ResponseEntity<String> move(
            @PathVariable("gameId") String gameId,
            @PathVariable("emailId") String emailId,
            @PathVariable("move") String move) {

        //check valid turn, valid move
        return new ResponseEntity<>("movment status", HttpStatus.OK);
    }

    @RequestMapping(value = "/quit/{gameId}/{emailId}", method = RequestMethod.GET)
    public ResponseEntity<String> leaveGame(
            @PathVariable("gameId") String gameId,
            @PathVariable("emailId") String emailId) {

        //close game
        return new ResponseEntity<>("game status updated", HttpStatus.OK);
    }


    @RequestMapping(value = "/view/{emailId}", method = RequestMethod.GET)
    public ResponseEntity<String> viewGames(
            @PathVariable("emailId") String emailId) {

        //display all game of the users
        return new ResponseEntity<>("display all games", HttpStatus.OK);
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ResponseEntity<String> viewAllGames() {

        //display all games irrespective of user
        return new ResponseEntity<>("display all games", HttpStatus.OK);
    }
}
