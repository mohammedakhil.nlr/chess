package com.sapient.chess.chess.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DummyController {
    @RequestMapping("/")
    public @ResponseBody String welcome(){
        return "Hello";
    }
}
