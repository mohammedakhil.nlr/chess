package com.sapient.chess.chess.dao;

public class DatabaseConstants {
    public static final String COLUMN_VALUE_FLAG_ACTIVE = "Y";
    public static final String COLUMN_VALUE_FLAG_INACTIVE = "N";

    public static final String COLUMN_VALUE_FLAG_ENABLED = "1";
    public static final String COLUMN_VALUE_FLAG_DISABLED = "0";

    private DatabaseConstants() {
    }
}
