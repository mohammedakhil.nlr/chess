package com.sapient.chess.chess.dao.impl;

import com.sapient.chess.chess.dao.UserGameDataDao;
import com.sapient.chess.chess.exception.DataCreationException;
import com.sapient.chess.chess.model.User;
import com.sapient.chess.chess.model.UserGameData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserGameDataDaoImpl implements UserGameDataDao {
    private static final Log LOG = LogFactory.getLog(UserGameDataDaoImpl.class);

    private final NamedParameterJdbcOperations jdbcOperations;
    private final UserGameRowMapper userGameRowMapper;


    @Inject
    public UserGameDataDaoImpl(DataSource dataSource, UserGameRowMapper userGameRowMapper) {
        jdbcOperations = new NamedParameterJdbcTemplate(dataSource);
        this.userGameRowMapper = userGameRowMapper;
    }

    private static final String QUERY_INSERT_USER_GAME_DATA =
            "INSERT INTO user_game_data (user_game_id, game_id, email_id, colour, result) " +
                    "VALUES (:id, :gameId, :emailId, :colour, :result)";

    @Override
    public void add(UserGameData userGameData) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", userGameData.getId());
        parameters.put("gameId", userGameData.getGameData().getGameId());
        parameters.put("emailId", userGameData.getUser().getEmail());
        parameters.put("colour", userGameData.getColor());
        parameters.put("result", userGameData.getResult());
        int rowsAdded = jdbcOperations.update(QUERY_INSERT_USER_GAME_DATA, parameters);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Inserted User " + userGameData + " (" + rowsAdded + " rows added)");
        }
        if (rowsAdded != 1) {
            throw new DataCreationException("Expected to add 1 record, but was " + rowsAdded);
        }
    }

    @Override
    public List<UserGameData> getByUser(User user) {
        return null;
    }

    private static final String GET_GAME_BY_GAME_STATUS_AND_COLOUR =
            "SELECT ugd.user_game_id, ugd.game_id, ugd.email_id, ugd.colour, ugd.result FROM user_game_data ugd " +
                    "INNER JOIN game_data gd ON gd.game_id = ugd.game_id " +
                    "WHERE ugd.colour = :colour AND gd.game_status = :gameStatus ";


    @Override
    public UserGameData getGameByGameStatusAndColour(String gameStatus, String colour) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("colour", colour);
        parameters.put("gameStatus", gameStatus);

        List<UserGameData> userGameDataList = jdbcOperations.query(GET_GAME_BY_GAME_STATUS_AND_COLOUR, parameters,
                userGameRowMapper);
        UserGameData userGameData;
        if (userGameDataList.size() == 0) {
            userGameData = null;
        } else {
            userGameData = userGameDataList.get(0);
        }

        return userGameData;
    }

    @Override
    public List<UserGameData> getByUserAndResult(User user, String result) {
        return null;
    }

    @Override
    public List<UserGameData> getByUserAndColor(User user, String color) {
        return null;
    }

    @Override
    public List<UserGameData> getByUserResultAndColor(User user, String result, String color) {
        return null;
    }
}
