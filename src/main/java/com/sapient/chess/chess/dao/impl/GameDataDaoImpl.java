package com.sapient.chess.chess.dao.impl;

import com.sapient.chess.chess.dao.GameDataDao;
import com.sapient.chess.chess.exception.DataCreationException;
import com.sapient.chess.chess.exception.DataUpdateException;
import com.sapient.chess.chess.model.GameData;
import com.sapient.chess.chess.model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static com.sapient.chess.chess.dao.DatabaseConstants.COLUMN_VALUE_FLAG_ACTIVE;
import static com.sapient.chess.chess.dao.DatabaseConstants.COLUMN_VALUE_FLAG_INACTIVE;

@Service
public class GameDataDaoImpl implements GameDataDao {
    private static final Log LOG = LogFactory.getLog(GameDataDaoImpl.class);

    private final NamedParameterJdbcOperations jdbcOperations;

    private static final RowMapper<GameData> GAME_ROW_MAPPER = new RowMapper<GameData>() {
        @Override
        public GameData mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            String id = resultSet.getString("game_id");
            String status = resultSet.getString("game_status");
            return new GameData(id, status);
        }
    };

    @Inject
    public GameDataDaoImpl(DataSource dataSource) {
        jdbcOperations = new NamedParameterJdbcTemplate(dataSource);
    }


    private static final String QUERY_INSERT_GAME =
            "INSERT INTO game_data (game_id, game_status) " +
                    "VALUES (:id, :status)";
    @Override
    public void addGame(GameData gameData) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", gameData.getGameId());
        parameters.put("status", gameData.getGameStatus());
        int rowsAdded = jdbcOperations.update(QUERY_INSERT_GAME, parameters);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Inserted Game " + gameData + " (" + rowsAdded + " rows added)");
        }
        if (rowsAdded != 1) {
            throw new DataCreationException("Expected to add 1 record, but was " + rowsAdded);
        }
    }

    private static final String QUERY_UPDATE_GAME_STATUS = "UPDATE game_data SET game_status = :newStatus " +
            "WHERE game_id = :id AND game_status = :oldStatus ";


    @Override
    public void updateGameStatus(GameData gameData, String oldStatus, String newStatus) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("newStatus", newStatus);
        parameters.put("id", gameData.getGameId());
        parameters.put("oldStatus", oldStatus);

        int rowsUpdated = jdbcOperations.update(QUERY_UPDATE_GAME_STATUS, parameters);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Updated game status " + newStatus + " (" + rowsUpdated + " rows updated)");
        }

        if (rowsUpdated != 1) {
            throw new DataUpdateException("Expected to update 1 record, but was " + rowsUpdated);
        }
    }

    private static final String QUERY_GET_ALL_GAMES = "SELECT game_id, game_status FROM game_data";


    @Override
    public Set<GameData> getAllGames() {
        List<GameData> gameDataList = jdbcOperations.query(QUERY_GET_ALL_GAMES, Collections.<String, Object>emptyMap(),
                GAME_ROW_MAPPER);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Loaded GameData from database " + gameDataList);
        }
        Set<GameData> gameDataSet = new HashSet<>(gameDataList);
        Set<GameData> immutableSet = Collections.unmodifiableSet(gameDataSet);
        return immutableSet;
    }

    private static final String QUERY_GET_BY_STATUS = "SELECT game_id, game_status FROM game_data " +
            "WHERE game_status = :gameStatus" ;

    @Override
    public List<GameData> getByStatus(String status) {
        return jdbcOperations.query(QUERY_GET_BY_STATUS, Collections.singletonMap("gameStatus", status),
                GAME_ROW_MAPPER);
    }

    private static final String QUERY_GET_GAME_DATA_BY_ID =
            "SELECT game_id, game_status FROM game_data WHERE game_id = :id";

    @Override
    public GameData getGameData(String gameId) {
        List<GameData> gameDataList = jdbcOperations.query(QUERY_GET_GAME_DATA_BY_ID, Collections.singletonMap("id",
                gameId), GAME_ROW_MAPPER);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Searching for Game Id " + gameId + " found Games " + gameDataList);
        }
        GameData gameData;
        if (gameDataList.size() == 0) {
            gameData = null;
        } else if (gameDataList.size() == 1) {
            gameData = gameDataList.get(0);
        } else {
            //this should never occur, being prevented by database constraints
            throw new IncorrectResultSizeDataAccessException("Multiple Games objects matched id " + gameId + ": " +
                    gameDataList, 1);
        }
        return gameData;
    }
}
