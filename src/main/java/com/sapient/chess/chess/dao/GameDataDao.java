package com.sapient.chess.chess.dao;

import com.sapient.chess.chess.model.GameData;

import java.util.List;
import java.util.Set;

public interface GameDataDao {
    void addGame(GameData gameData);

    void updateGameStatus(GameData gameData, String oldStatus, String newStatus);

    GameData getGameData(String gameId);

    Set<GameData> getAllGames();

    List<GameData> getByStatus(String status);
}
