package com.sapient.chess.chess.dao;

import com.sapient.chess.chess.model.User;

import java.util.Set;

public interface UserDao {
    void add(User user);

    User getUser(String emailId);

    Set<User> getUsers();

    Set<String> getUserIds();
}
