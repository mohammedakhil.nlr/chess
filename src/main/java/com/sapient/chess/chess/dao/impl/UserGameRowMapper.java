package com.sapient.chess.chess.dao.impl;

import com.sapient.chess.chess.dao.GameDataDao;
import com.sapient.chess.chess.dao.UserDao;
import com.sapient.chess.chess.model.UserGameData;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class UserGameRowMapper implements RowMapper<UserGameData> {
    private final UserDao userDao;
    private final GameDataDao gameDataDao;

    @Inject
    public UserGameRowMapper(UserDao userDao, GameDataDao gameDataDao) {
        this.userDao = userDao;
        this.gameDataDao = gameDataDao;
    }

    @Override
    public UserGameData mapRow(ResultSet resultSet, int i) throws SQLException {
        String id = resultSet.getString("user_game_id");
        String gameId = resultSet.getString("game_id");
        String userId = resultSet.getString("email_id");
        String colour = resultSet.getString("colour");
        String result = resultSet.getString("result");
        return new UserGameData(id, userDao.getUser(userId), gameDataDao.getGameData(gameId), colour, result);
    }
}
