package com.sapient.chess.chess.dao.impl;

import com.sapient.chess.chess.dao.UserDao;
import com.sapient.chess.chess.exception.DataCreationException;
import com.sapient.chess.chess.model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.sapient.chess.chess.dao.DatabaseConstants.COLUMN_VALUE_FLAG_ACTIVE;
import static com.sapient.chess.chess.dao.DatabaseConstants.COLUMN_VALUE_FLAG_INACTIVE;

@Service
public class UserDaoImpl implements UserDao {
    private static final Log LOG = LogFactory.getLog(UserDaoImpl.class);

    private final NamedParameterJdbcOperations jdbcOperations;

    private static final RowMapper<User> USER_ROW_MAPPER = new RowMapper<User>() {
        @Override
        public User mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            String email = resultSet.getString("email_id");
            String password = resultSet.getString("password");
            boolean active = COLUMN_VALUE_FLAG_ACTIVE.equals(resultSet.getString("active_flag"));
            return new User(password, email, active);
        }
    };

    @Inject
    public UserDaoImpl(DataSource dataSource) {
        jdbcOperations = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final String QUERY_INSERT_USER =
            "INSERT INTO dim_user (email_id, password, active_flag) " +
                    "VALUES (:id, :password, :active)";

    @Override
    public void add(User user) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", user.getEmail());
        parameters.put("password", user.getPassword());
        parameters.put("active", user.isActive() ? COLUMN_VALUE_FLAG_ACTIVE : COLUMN_VALUE_FLAG_INACTIVE);
        int rowsAdded = jdbcOperations.update(QUERY_INSERT_USER, parameters);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Inserted User " + user + " (" + rowsAdded + " rows added)");
        }
        if (rowsAdded != 1) {
            throw new DataCreationException("Expected to add 1 record, but was " + rowsAdded);
        }
    }

    private static final String QUERY_GET_USER_BY_EMAIL_ID =
            "SELECT email_id, password, active_flag FROM dim_user WHERE email_id = :id";

    @Override
    public User getUser(String id) {
        List<User> users = jdbcOperations.query(QUERY_GET_USER_BY_EMAIL_ID, Collections.singletonMap("id", id),
                USER_ROW_MAPPER);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Searching for id " + id + " found users " + users);
        }
        User user;
        if (users.size() == 0) {
            user = null;
        } else if (users.size() == 1) {
            user = users.get(0);
        } else {
            //this should never occur, being prevented by database constraints
            throw new IncorrectResultSizeDataAccessException("Multiple Users objects matched id " + id + ": " + users,
                    1);
        }
        return user;
    }


    private static final String QUERY_GET_ALL_USERS = "SELECT email_id, password, active_flag FROM dim_user";

    @Override
    public Set<User> getUsers() {
        List<User> users = jdbcOperations.query(QUERY_GET_ALL_USERS, Collections.<String, Object>emptyMap(),
                USER_ROW_MAPPER);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Loaded Users from database " + users);
        }
        Set<User> userSet = new HashSet<>(users);
        Set<User> immutableUsers = Collections.unmodifiableSet(userSet);
        return immutableUsers;
    }

    private static final String QUERY_GET_ALL_USERS_IDS =
            "SELECT email_id FROM dim_user";

    @Override
    public Set<String> getUserIds() {
        List<String> userIds = jdbcOperations.queryForList(QUERY_GET_ALL_USERS_IDS, Collections.<String, Object>emptyMap(),
                String.class);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Loaded User ids from database " + userIds);
        }
        Set<String> userdIdSet = new HashSet<>(userIds);
        Set<String> immutableUserds = Collections.unmodifiableSet(userdIdSet);
        return immutableUserds;
    }
}
