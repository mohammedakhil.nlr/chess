package com.sapient.chess.chess.dao;

import com.sapient.chess.chess.model.User;
import com.sapient.chess.chess.model.UserMoveData;

import java.util.List;

public interface UserMoveDao {
    void add(UserMoveData userMoveData);

    UserMoveData getLastMove(User user);

    List<UserMoveDao> getAllMoves(User user);
}
