package com.sapient.chess.chess.dao;

import com.sapient.chess.chess.model.GameData;
import com.sapient.chess.chess.model.User;
import com.sapient.chess.chess.model.UserGameData;

import java.util.List;

public interface UserGameDataDao {
    void add(UserGameData userGameData);

    List<UserGameData> getByUser(User user);

    UserGameData getGameByGameStatusAndColour(String gameStatus, String colour);

    List<UserGameData> getByUserAndResult(User user, String result);

    List<UserGameData> getByUserAndColor(User user, String color);

    List<UserGameData> getByUserResultAndColor(User user, String result, String color);
}
