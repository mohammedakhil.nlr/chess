package com.sapient.chess.chess.exception;

public class DataExistsException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public DataExistsException(String message) {
        super(message);
    }

    public DataExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
