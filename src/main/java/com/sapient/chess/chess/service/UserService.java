package com.sapient.chess.chess.service;

import com.sapient.chess.chess.model.User;
import com.sapient.chess.chess.requests.UserRequest;

import java.util.Set;

public interface UserService {
    void addUser(UserRequest userRequest);
    User getUser(String emailId);

    Set<User> getAllUsers();

    boolean doesUserExists(String emailId);
}
