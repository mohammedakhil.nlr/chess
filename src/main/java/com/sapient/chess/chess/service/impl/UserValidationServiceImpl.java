package com.sapient.chess.chess.service.impl;

import com.sapient.chess.chess.dao.UserDao;
import com.sapient.chess.chess.exception.InvalidDataException;
import com.sapient.chess.chess.requests.UserRequest;
import com.sapient.chess.chess.service.UserService;
import com.sapient.chess.chess.service.UserValidationService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserValidationServiceImpl implements UserValidationService {
    private static final Pattern EMAIL_PATTERN = Pattern.compile("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b");
    private static final int MAXIMUM_LENGTH_PASSWORD = 20;
    private static final int MAXIMUM_LENGTH_EMAIL = 64;
    private final UserDao userDao;

    @Inject
    public UserValidationServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void validate(UserRequest userRequest) {
        validateEmailId(userRequest.getEmailId());

        if (StringUtils.isEmpty(userRequest.getPassword())) {
            throw new InvalidDataException("Password is required.");
        }

        String password = userRequest.getPassword();
        validateLength(password, MAXIMUM_LENGTH_PASSWORD, "Password");
    }

    @Override
    public void validate(String emailId) {
        validateEmailId(emailId);
        if(!userDao.getUserIds().contains(emailId)){
            throw new InvalidDataException("User Not Registered");
        }
    }

    private void validateEmailId(String emailId) {
        if (StringUtils.isEmpty(emailId)) {
            throw new InvalidDataException("Email id is required.");
        }

        validateIdentifier(emailId);
        validateLength(emailId, MAXIMUM_LENGTH_EMAIL, "Email");
    }

    private void validateLength(String value, int maximumLength, String propertyDescription) {
        if (value.length() > maximumLength) {
            throw new InvalidDataException(propertyDescription + " \"" + value +
                    "\" exceeds maximum length " + maximumLength);
        }
    }

    private void validateIdentifier(String identifier) {
        Matcher matcher = EMAIL_PATTERN.matcher(identifier);
        if (!matcher.matches()) {
            throw new InvalidDataException("\"" + identifier + "\" is an invalid email id.");
        }
    }
}
