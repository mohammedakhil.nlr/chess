package com.sapient.chess.chess.service.impl;

import com.sapient.chess.chess.dao.UserDao;
import com.sapient.chess.chess.exception.DataExistsException;
import com.sapient.chess.chess.model.User;
import com.sapient.chess.chess.requests.UserRequest;
import com.sapient.chess.chess.service.UserService;
import com.sapient.chess.chess.service.UserValidationService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    private final UserDao userDao;
    private final UserValidationService userValidationService;

    @Inject
    public UserServiceImpl(UserDao userDao, UserValidationService userValidationService) {
        this.userDao = userDao;
        this.userValidationService = userValidationService;
    }

    @Override
    public void addUser(UserRequest userRequest) {
        if (doesUserExists(userRequest.getEmailId())) {
            throw new DataExistsException("A user with email id " + userRequest.getEmailId() + " already exists.");
        }
        userValidationService.validate(userRequest);
        userDao.add(new User(userRequest.getPassword(), userRequest.getEmailId(), true));
    }

    @Override
    public boolean doesUserExists(String emailId) {
        return userDao.getUserIds().contains(emailId);
    }

    @Override
    public Set<User> getAllUsers() {
        return userDao.getUsers();
    }

    @Override
    public User getUser(String emailId) {
        return userDao.getUser(emailId);
    }
}
