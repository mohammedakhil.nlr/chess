package com.sapient.chess.chess.service;

public interface GameService {
    String assignGame(String emailId, String colour);

}
