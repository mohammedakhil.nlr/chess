package com.sapient.chess.chess.service;

public interface GameServiceValidator {
    void validate(String emailId, String color);
}
