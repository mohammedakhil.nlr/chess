package com.sapient.chess.chess.service.impl;

import com.sapient.chess.chess.api.Colour;
import com.sapient.chess.chess.api.GameStatus;
import com.sapient.chess.chess.dao.GameDataDao;
import com.sapient.chess.chess.dao.UserGameDataDao;
import com.sapient.chess.chess.model.GameData;
import com.sapient.chess.chess.model.User;
import com.sapient.chess.chess.model.UserGameData;
import com.sapient.chess.chess.service.GameService;
import com.sapient.chess.chess.service.GameServiceValidator;
import com.sapient.chess.chess.service.UserService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class GameServiceImpl implements GameService {
    private final GameServiceValidator gameServiceValidator;
    private final GameDataDao gameDataDao;
    private final UserGameDataDao userGameDataDao;
    private static final AtomicInteger USER_GAME_COUNTER = new AtomicInteger(new Random().nextInt(999999));
    private static final AtomicInteger GAME_COUNTER = new AtomicInteger(new Random().nextInt(999999));

    @Inject
    public GameServiceImpl(GameServiceValidator gameServiceValidator, GameDataDao gameDataDao,
                           UserGameDataDao userGameDataDao, UserService userService) {
        this.gameServiceValidator = gameServiceValidator;
        this.gameDataDao = gameDataDao;
        this.userGameDataDao = userGameDataDao;
    }

    @Override
    public String assignGame(String emailId, String colour) {
        gameServiceValidator.validate(emailId, colour);
        UserGameData userGameDataExisting = userGameDataDao.getGameByGameStatusAndColour(GameStatus.Open.getCode(),
                Colour.valueOf(colour).equals(Colour.BLACK) ? Colour.WHITE.name() : Colour.BLACK.name());
        boolean gameIndicator = false;
        String gameId;

        if (userGameDataExisting == null) {
            //Create New Game
            gameId = createGameData(emailId, colour);
        } else {
            //player waiting for game and assign this player to the game
            GameData gameData = userGameDataExisting.getGameData();
            UserGameData userGameData = new UserGameData(
                    String.valueOf(USER_GAME_COUNTER.incrementAndGet()),
                    new User(null, emailId, true),
                    gameData,
                    Colour.valueOf(colour).name(), null);
            userGameDataDao.add(userGameData);
            gameDataDao.updateGameStatus(gameData, gameData.getGameStatus(), GameStatus.Playing.getCode());
            gameId = gameData.getGameId();
            gameIndicator = true;
        }

        if (gameIndicator) {
            return "Start Game with gameId: " + gameId;
        } else {
            return "Waiting for player to join game: " + gameId;
        }
    }

    private String createGameData(String emailId, String colour) {
        GameData gameData = new GameData(String.valueOf(GAME_COUNTER.incrementAndGet()), GameStatus.Open.getCode());
        gameDataDao.addGame(gameData);
        UserGameData userGameData = new UserGameData(
                String.valueOf(USER_GAME_COUNTER.incrementAndGet()),
                new User(null, emailId, true),
                gameData,
                Colour.valueOf(colour).name(), null);
        userGameDataDao.add(userGameData);
        return gameData.getGameId();
    }
}
