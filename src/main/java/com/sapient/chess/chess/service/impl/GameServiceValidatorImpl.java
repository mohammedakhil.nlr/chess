package com.sapient.chess.chess.service.impl;

import com.sapient.chess.chess.api.Colour;
import com.sapient.chess.chess.exception.InvalidDataException;
import com.sapient.chess.chess.service.GameServiceValidator;
import com.sapient.chess.chess.service.UserValidationService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class GameServiceValidatorImpl implements GameServiceValidator {
    private final UserValidationService userValidationService;

    @Inject
    public GameServiceValidatorImpl(UserValidationService userValidationService) {
        this.userValidationService = userValidationService;
    }

    @Override
    public void validate(String emailId, String colour) {
        userValidationService.validate(emailId);
        if(Colour.valueOf(colour) == null){
            throw new InvalidDataException("Invalid colour Available Colours: " + Colour.BLACK + ", " + Colour.WHITE);
        }
    }
}
