package com.sapient.chess.chess.service;

import com.sapient.chess.chess.requests.UserRequest;

import java.util.regex.Pattern;

public interface UserValidationService {
    void validate(UserRequest userRequest);

    void validate(String emailId);
}
